using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class InteractionManager : MonoBehaviour
{
    public Text textObject;



    public GameObject SelectedObject;
    public List<int> ItemIDs;
    // Start is called before the first frame update
    void Start()
    {


        ItemIDs = new List<int> { };

        textObject.text = "";

    }

    // Update is called once per frame
    void Update()
    {

        Camera CameraObject = gameObject.GetComponent<Camera>();
        Ray ray = CameraObject.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 2f))
        {
            SelectedObject = hit.transform.gameObject;
            if(SelectedObject.tag == "Interact")
            {
                string message = SelectedObject.GetComponent<InteractionParent>().Communicate();
                textObject.text = message;

                if(Input.GetKeyDown("e"))
                {
                    ItemIDs = SelectedObject.GetComponent<InteractionParent>().Activate();
                }
            }
            else
            {
                    textObject.text = "";
            }

        }
        else
        {
            textObject.text = "";
        }
    }
}