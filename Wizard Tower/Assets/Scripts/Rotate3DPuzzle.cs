using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate3DPuzzle : MonoBehaviour
{
    public GameObject rightRotateArrow;
    public GameObject leftRotateArrow;
    //Rigidbody mazeBoxRigidbody;
    bool isBoxRotatingRight = false;
    bool isBoxRotatingLeft = false;

    float timeToCompleteRotation = 1.0f;
    float currentRotationTime = 0f;
    float rotationCompletePercentage = 0;
    public GameObject rotateAround;
    float startYRotation;




    // Start is called before the first frame update
    void Start()
    {
        
        //mazeBoxRigidbody = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if (isBoxRotatingLeft == false && isBoxRotatingRight == false)
            {
                if (CameraRaycast.currentHitInteractable == rightRotateArrow)
                {
                    isBoxRotatingRight = true;
                    startYRotation = transform.rotation.eulerAngles.y;
                }
                else if (CameraRaycast.currentHitInteractable == leftRotateArrow)
                {
                    isBoxRotatingLeft = true;
                    startYRotation = transform.rotation.eulerAngles.y;
                }
            }            
        }

        if (isBoxRotatingLeft == true)
        {
            currentRotationTime += Time.deltaTime;
            rotationCompletePercentage = currentRotationTime / timeToCompleteRotation;

            if (currentRotationTime < timeToCompleteRotation)
            {                   
                transform.SetPositionAndRotation(gameObject.transform.position, Quaternion.Euler(0, startYRotation + 90 * rotationCompletePercentage, 0));                
            }
            else
            {
                transform.SetPositionAndRotation(gameObject.transform.position, Quaternion.Euler(0, startYRotation + 90, 0));

                isBoxRotatingLeft = false;
                currentRotationTime = 0;
                rotationCompletePercentage = 0;
            }
        }
        else if (isBoxRotatingRight == true)
        {
            currentRotationTime += Time.deltaTime;
            rotationCompletePercentage = currentRotationTime / timeToCompleteRotation;

            if (currentRotationTime < timeToCompleteRotation)
            {
                transform.SetPositionAndRotation(gameObject.transform.position, Quaternion.Euler(0, startYRotation - 90 * rotationCompletePercentage, 0));
            }
            else
            {
                transform.SetPositionAndRotation(gameObject.transform.position, Quaternion.Euler(0, startYRotation - 90, 0));

                isBoxRotatingRight = false;
                currentRotationTime = 0;
                rotationCompletePercentage = 0;
            }
        }
    }
}
