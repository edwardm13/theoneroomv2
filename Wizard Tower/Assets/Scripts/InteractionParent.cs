using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InteractionParent : MonoBehaviour
{
    // the parent of the interaction classes


    public string CurrentMessage = "E - Interact";
    public string AlternateMessage = "Hello!";

    public GameObject Player;
    public AudioSource AudioSource;

    public AudioClip CurrentClip;
    public AudioClip AudioClip;
    public AudioClip AlternativeClip;


    void Start()
    {
   //     Debug.Log("Parent");

        Player = GameObject.Find("Player");
        AudioSource = Player.GetComponent<AudioSource>();

        CurrentClip = AudioClip;
    }

    public string Communicate()
    {
        return CurrentMessage;
    }

    public virtual List<int> Activate()
    {
        CurrentMessage = AlternateMessage;
        AudioSource.PlayOneShot(CurrentClip);
        CurrentClip = AlternativeClip;

        return new List<int> { };
    }
    // Start is called before the first frame update


    // Update is called once per frame
    void Update()
    {


    }

}
